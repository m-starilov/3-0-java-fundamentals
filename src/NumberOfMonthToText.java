public class NumberOfMonthToText {

  public static void main(String[] args) {
    String[] months = new String[12];
    months[0] = "January";
    months[1] = "February";
    months[2] = "March";
    months[3] = "April";
    months[4] = "May";
    months[5] = "June";
    months[6] = "July";
    months[7] = "August";
    months[8] = "September";
    months[9] = "October";
    months[10] = "November";
    months[11] = "December";
    int number;
    try {
      number = Integer.parseInt(args[0]);
    } catch (NumberFormatException e) {
      throw new NumberFormatException("Only numbers!");
    }
    if (number > 0 && number < 13) {
      System.out.println(months[number - 1]);
    } else {
      System.out.println("Only numbers from 1 to 12!");
    }
  }

}
