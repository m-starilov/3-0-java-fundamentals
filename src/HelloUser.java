public class HelloUser {

  public static void main(String[] args) {
    String name = args[0];
    System.out.printf("Hello, %1$s!", name);
  }

}
