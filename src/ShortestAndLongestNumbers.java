public class ShortestAndLongestNumbers {

  public static void main(String[] args) {
    int[] numbers = new int[args.length];
    try {
      for (int i = 0; i < args.length; i++) {
        numbers[i] = Integer.parseInt(args[i]);
      }
    } catch (NumberFormatException e) {
      throw new NumberFormatException("Only numbers!");
    }
    int shortestNumber = numbers[0];
    int longestNumber = numbers[0];
    for (Integer number : numbers) {
      if (number < shortestNumber) {
        shortestNumber = number;
      } else if (number > longestNumber) {
        longestNumber = number;
      }
    }
    System.out.printf("The shortest number is - %1$s it length is - %2$s\n",
        shortestNumber, Integer.toString(shortestNumber).length());
    System.out.printf("The longest number is %1$s it length is - %2$s\n",
        longestNumber, Integer.toString(longestNumber).length());
  }

}
