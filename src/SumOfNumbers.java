public class SumOfNumbers {

  public static void main(String[] args) {
    int sum = 0;
    int prod = 1;
    int number;
    for (String arg : args) {
      try {
        number = Integer.parseInt(arg);
      } catch (NumberFormatException e) {
        throw new NumberFormatException("Only numbers!");
      }
      sum += number;
      prod *= number;
    }
    System.out.println("Sum of numbers = " + sum);
    System.out.println("Product of numbers = " + prod);
  }

}
