import java.util.Random;

public class OutputNumbers {

  public static void main(String[] args) {
    int n;
    try {
      n = Integer.parseInt(args[0]);
    } catch (NumberFormatException e) {
      throw new NumberFormatException("Only numbers!");
    }
    int[] array = new Random().ints(Integer.toUnsignedLong(n), 0, 100).toArray();
    System.out.println(n + " random numbers in multiple lines:");
    for (int i = 0; i < n; i++) {
      System.out.println(array[i]);
    }
    System.out.println(n + " random numbers in a single line:");
    for (int i = 0; i < n; i++) {
      System.out.print(array[i] + " ");
    }
  }

}